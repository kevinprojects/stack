//
//  EmptyNode.swift
//  StackDemo
//
//  Created by Kevin Belter on 2/10/15.
//  Copyright © 2015 Kevin Belter. All rights reserved.
//

import Foundation

class EmptyNode<T>: Node<T> {
    
    init() {
        super.init(fatherNode: nil, object: nil)
    }
    
    override func pop() throws -> Node<T> {
        throw Error.StackIsEmptyException
    }
    
    override func isEmpty() -> Bool {
        return true
    }
    
    override func size() -> Int {
        return 0
    }
    
    override func getObject() throws -> T {
        throw Error.StackIsEmptyException
    }
    
    override func setObject(object: T) {
        super.setObject(object)
    }
}