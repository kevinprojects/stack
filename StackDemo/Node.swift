//
//  Node.swift
//  StackDemo
//
//  Created by Kevin Belter on 2/10/15.
//  Copyright © 2015 Kevin Belter. All rights reserved.
//

import Foundation

class Node<T> {
    
    private var _object: T?
    private var _fatherNode: Node<T>?
    
    init(fatherNode: Node<T>?, object: T?) {
        _object = object
        _fatherNode = fatherNode
    }
    
    var fatherNode: Node<T> {
        get {
            return _fatherNode!
        }
        set {
            _fatherNode = newValue
        }
    }
    
    func pop() throws -> Node<T> {throw Error.NotImplementedException }
    func isEmpty() -> Bool {return false}
    func size() -> Int {return 0}
    func getObject() throws -> T {return _object!}
    func setObject(object: T) {_object = object}
}