//
//  NodeWithObject.swift
//  StackDemo
//
//  Created by Kevin Belter on 2/10/15.
//  Copyright © 2015 Kevin Belter. All rights reserved.
//

import Foundation

class NodeWithObject<T>: Node<T>   {
    
    override init(fatherNode: Node<T>?, object: T?) {
        super.init(fatherNode: fatherNode, object: object)
    }
    
    override func pop() throws -> Node<T> {
        return super.fatherNode
    }
    
    override func isEmpty() -> Bool {
        return false
    }
    
    override func size() -> Int {
        return super.fatherNode.size() + 1
    }
    
    override func getObject() throws -> T {
        return try super.getObject()
    }
    
    override func setObject(object: T) {
        super.setObject(object)
    }
}