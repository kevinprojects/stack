//
//  Stack.swift
//  StackDemo
//
//  Created by Kevin Belter on 2/10/15.
//  Copyright © 2015 Kevin Belter. All rights reserved.
//

import Foundation

enum Error: ErrorType {
    case StackIsEmptyException
    case NotImplementedException
}

public class Stack<T> {
    
    private var _baseNode: Node<T>
    
    init() {
        _baseNode = EmptyNode()
    }
    
    func push(anObject: T) {
        _baseNode = NodeWithObject(fatherNode: _baseNode, object: anObject)
    }
    
    func pop() throws -> T {
        let object = try _baseNode.getObject()
        _baseNode = try _baseNode.pop()
        return object
    }
    
    func top() throws -> T {
        return try _baseNode.getObject()
    }
    
    func isEmpty() -> Bool {
        return _baseNode.isEmpty()
    }
    
    func size() -> Int {
        return _baseNode.size()
    }
}