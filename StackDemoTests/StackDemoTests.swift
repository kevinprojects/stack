//
//  StackDemoTests.swift
//  StackDemoTests
//
//  Created by Kevin Belter on 29/2/16.
//  Copyright © 2016 kevinbelter. All rights reserved.
//

import XCTest
@testable import StackDemo

class StackDemoTests: XCTestCase {
    
    func testTheStackMustByEmptyWhenCreated() {
        let stack = Stack<String>()
        XCTAssertTrue(stack.isEmpty())
    }
    
    func testYouCanPushItemsIntoTheStack() {
        let stack = Stack<Int>()
        stack.push(1)
        XCTAssertFalse(stack.isEmpty())
    }
    
    func testWhenYouPopTheItemComesOutTheStack() {
        let stack = Stack<Float>()
        stack.push(10.5)
        do {
            try stack.pop()
            XCTAssertTrue(stack.isEmpty())
        } catch {
            XCTAssertTrue(false, "Exception pop")
        }
    }
    
    func testWhenPopIGetTheLastItemFromTheStack() {
        let stack = Stack<Double>()
        let doubleToPush = 10.1
        stack.push(doubleToPush)
        
        do {
            let popObject = try stack.pop()
            XCTAssertEqual(doubleToPush, popObject)
        } catch {
            XCTAssertTrue(false, "Exception on pop")
        }
    }
    
    func testTheStackHasAGoodBehavior() {
        let firstPush = "First"
        let secondPush = "Second"
        
        let stack = Stack<String>()
        stack.push(firstPush)
        stack.push(secondPush)
        
        do {
            let firstPop = try stack.pop()
            XCTAssertEqual(secondPush, firstPop)
        } catch {
            XCTAssertTrue(false, "Exception on pop")
        }
        
        do {
            let secondPop = try stack.pop()
            XCTAssertEqual(firstPush, secondPop)
        } catch {
            XCTAssertTrue(false, "Exception on pop")
        }
        
        XCTAssertTrue(stack.isEmpty())
    }
    
    func testTopDontRemoveItemFromStack() {
        let stack = Stack<String>()
        let pushedObject = "Something"
        stack.push(pushedObject)
        
        XCTAssertEqual(1, stack.size())
        
        do {
            try stack.top()
        } catch {
            XCTAssertTrue(false, "Exception on top")
        }
        
        XCTAssertEqual(1, stack.size())
    }
    
    
    func testYouCantPopWhenNoItemsInStack() {
        let stack = Stack<Int>()
        do {
            try stack.pop()
            XCTFail()
        } catch Error.StackIsEmptyException {
            XCTAssertTrue(true)
        } catch {
            XCTAssertTrue(false, "Not exploded when pop on empty stack")
        }
    }
    
    func testCanNotTopWhenNoItemsInTheStack() {
        let stack = Stack<Int>()
        do {
            try stack.top()
            XCTFail()
        } catch Error.StackIsEmptyException {
            XCTAssertTrue(true)
        } catch {
            XCTAssertTrue(false, "Not exploded when top on empty stack")
        }
    }
}
